package com.lfg.annuity.csi.batch.launcher.lambda;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.lfg.annuity.csi.batch.constants.CSIBatchLauncherConstants;


import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sfn.SfnClient;
import software.amazon.awssdk.services.sfn.model.SfnException;
import software.amazon.awssdk.services.sfn.model.StartExecutionRequest;

public class CSIBatchLauncherHandler {

	private static final Logger log = LoggerFactory.getLogger(CSIBatchLauncherHandler.class);

	private final AmazonS3 amazons3 = AmazonS3ClientBuilder.defaultClient();
	String response = new String("200 Ok");

	public String handleRequest(S3Event s3Event, Context context) {
		
		log.info("inside CSI Batch Launcher handle request");
		System.out.println("Inside Batch Launcher Handle Request");
		
		try {
			s3Event.getRecords().forEach(record -> {
				String fileName = record.getS3().getObject().getKey();
				S3ObjectInputStream s3ObjectInputStream = amazons3.getObject(record.getS3().getBucket().getName(), fileName)
						.getObjectContent();
				log.info("CSI Batch Launcher Lambda is triggered by file");
				System.out.println("CSI Batch Launcher Lambda is triggered by file" + fileName);
				
				if(s3ObjectInputStream != null) {
					Region region = Region.US_EAST_1;
					String fileNameJson = JSONObject.toString(CSIBatchLauncherConstants.FILE_NAME, fileName);
					SfnClient sfnClient = SfnClient.builder().region(region).build();
					StartExecutionRequest startExecutionRequest = StartExecutionRequest.builder()
							.input(fileNameJson).stateMachineArn("arn:aws:states:us-east-1:994540997610:stateMachine:BlogBatchMainOrchestrator")
							.name("BlogBatchMainOrchestrator").build();
					sfnClient.startExecution(startExecutionRequest);
					
					log.info("The csi batch Launcher started successfully");
					System.out.println("The CSI batch Launcher started successfully");
				}
			});

		} catch (SfnException e) {
			log.error("Sfn Exception in Batch Launcher Lambda Logger" + e.getStackTrace());
			System.out.println("Sfn Exception in Batch Launcher Lambda" + e.getStackTrace());
			System.out.println("Sfn Exception in Batch Launcher Lambda" + e.getMessage());

		} catch (Exception e) {
			log.error("Exception in Batch Launcher Lambda Logger" + e.getStackTrace());
			System.out.println("Sfn Exception in Batch Launcher Lambda" + e.getMessage());
		}

		return response;
	}

}
